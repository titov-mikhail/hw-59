import React from 'react';
import './Button.css'

function Button(props) {
    return (
       <button className="Button" type="button" onClick={()=>props.onClick()}>{props.caption}</button>
    );
}

export default React.memo(Button, (props, nextProps)=>{
    return props.caption === nextProps.caption;
});
import React, {useEffect} from 'react';
import Image from "../Image/Image";

function Joke(props) {
    useEffect(()=>{}, )
    return (
        <div className="Joke">
          <Image
            iconUrl = {props.icon}
          />
           <p> {props.text}</p>
        </div>
    );
}

export default React.memo(Joke, (props, nextProps) =>{
   return props.icon === nextProps.icon && props.text === nextProps.text;
})
import React, {Component} from 'react';

class Image extends Component {

    shouldComponentUpdate(nextProps) {
        return this.props.iconUrl !== nextProps.iconUrl;
    }

    render() {
        return (
           <img src={this.props.iconUrl} alt='Chuck Norris'/>
        );
    }
}

export default Image;
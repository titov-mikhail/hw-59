import React, {useEffect, useState} from 'react';
import Joke from "../components/Joke/Joke";
import Button from "../components/Buttons/Button";
import './Jokes.css';

const url = "https://api.chucknorris.io/jokes/random";

const  Jokes = () => {

    const [jokes, setJokes] = useState([]);

     useEffect(() => {
         fetchJokes();
     }, [])

    const fetchJokes = ()=>{
        const requests = [];
        for(let i=0; i<5; i++) {
            const fetchData = async () => {
                const response = await fetch(url);

                if (response.ok) {
                    return await response.json();
                }
            };
            requests.push(fetchData().catch(e => console.error(e)));
        }

        Promise.all(requests).then(r => setJokes(r))
    }

    return (
        <div className="Jokes">
            <Button
                caption = "Get new jokes"
                onClick = {()=>fetchJokes()}
            />

            {jokes.map(j=>{
                return <Joke
                    key = {j.id}
                    icon = {j.icon_url}
                    text = {j.value}
                />
            })}
        </div>
    );
}

export default Jokes;
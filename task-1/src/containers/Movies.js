import React, {Component} from 'react';
import './Movies.css';
import AddMovie from "../components/AddMovie/AddMovie";
import MovieItem from "../components/Movie/MovieItem";

class Movies extends Component {
   constructor(props) {
       super(props);

       this.state = {
          movies: []
       }
   }

   onMovieAdded(movieName) {
       if(movieName.trim() === '')  return;

       const movie = {id: Date.now(), name: movieName};
       this.setState({ movies: [...this.state.movies, movie] });
   }

   onMovieRemoved(movieId){
       this.setState({movies: this.state.movies.filter((movie)=>{
            return movie.id !== movieId;
           })});
   }

   onMovieNameChanged(movieId, movieName) {
       this.setState({movies: this.state.movies.map(movie=>{
            if(movie.id === movieId) {
                return { id: movieId, name: movieName }
            }
            return  movie;
           })});
   }

    render() {
        return (

            <div className="Movies">
                    <AddMovie
                        onMovieAdded = {(movieName)=> this.onMovieAdded(movieName)}
                    />
                {this.state.movies.map(movie=> {
                   return <MovieItem
                        key ={movie.id}
                        movieId = { movie.id }
                        movieName= { movie.name }
                        onMovieRemoved = { (movieId)=>this.onMovieRemoved(movieId) }
                        onMovieNameChanged = { (movieId, movieName)=> this.onMovieNameChanged(movieId, movieName) }
                    />
               })}
            </div>
        );
    }
}

export default Movies;

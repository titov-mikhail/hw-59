import React, {Component} from 'react';
import  './MovieItem.css';
import RemoveButton from "../Buttons/RemoveButton";

class MovieItem extends Component {

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return this.props.movieName !== nextProps.movieName;
    }

    render() {
        return (
            <div className="MovieItem" >
                <input
                    type="text"
                    value={this.props.movieName}
                    name={"inp"+this.props.movieName}
                    onChange={(e)=>this.props.onMovieNameChanged(this.props.movieId, e.target.value)}/>
                <RemoveButton
                    onClick = {()=>this.props.onMovieRemoved(this.props.movieId)}
                />
            </div>
        );
    }
}

export default MovieItem;
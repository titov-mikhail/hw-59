import React, {Component} from 'react';

class AddButton extends Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return false;
    }

    render() {
        return (
            <button className="AddButton" type="button" onClick={(e)=>this.props.onClick(e)}>Add</button>
        );
    }
}

export default AddButton;
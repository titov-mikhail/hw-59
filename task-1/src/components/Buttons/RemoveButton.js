import React, {Component} from 'react';

class RemoveButton extends Component {
    shouldComponentUpdate() {
        return false;
    }

    render() {
        return (
           <button className="RemoveButton" type="button" onClick={(e=>this.props.onClick(e))}>X</button>
        );
    }
}

export default RemoveButton;
import React, {Component} from 'react';
import AddButton from "../Buttons/AddButton";
import './AddMovie.css';

class AddMovie extends Component {
    constructor(props) {
        super(props);
        this.state = {
            movieName: ''
        };
    }

    shouldComponentUpdate(nextProps) {
        return this.state.movieName !== nextProps.movieName;
    }

    movieNameChanged(movieName){
        this.setState({movieName: movieName});
    }

    movieNameAdded() {
        this.props.onMovieAdded(this.state.movieName);
        this.setState({movieName: ""})
    }
    render() {
        return (
            <div className="AddMovie" key="addMovie">
                <input
                    value={this.state.movieName}
                    type="text"
                    name="inputMovie"
                    onChange={(e) => this.movieNameChanged(e.target.value)}/>
                <AddButton
                    onClick = {()=>this.movieNameAdded()}
                />
            </div>
        );
    }
}

export default AddMovie;